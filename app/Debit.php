<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Account;
class Debit extends Model
{
     /**
     * @var string
     */
    protected $table = 'debits';

    /**
     * @var array
     */
    protected $guarded = [];


    public function account() 
    {
        return $this->belongsTo(Account::class);
    }
}