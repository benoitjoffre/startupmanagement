<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Account;
class Credit extends Model
{
     /**
     * @var string
     */
    protected $table = 'credits';

    /**
     * @var array
     */
    protected $guarded = [];


    public function account() 
    {
        return $this->belongsTo(Account::class);
    }
}