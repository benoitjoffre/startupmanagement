<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Account;
use App\Credit;
use App\Http\Requests\CreditFormRequest;


class CreditController extends Controller
{
  

    public function index() 
    {
        $credits = Credit::with('account')->get();

        return response()->json([
            'success' => true,
            'credits'  => $credits,
        ], 200);
    }

    /**
     * @param CreditFormRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(CreditFormRequest $request, $accountId)
    {
        $credit = new Credit();
        $credit->desc = $request->desc;
        $credit->amount = $request->amount;
        $credit->startDate = $request->startDate;

        // Get the account by accountId in url
        $account = Account::find($accountId);

        $credit->account()->associate($account);
        
        $credit->save();

            return response()->json([
                'success' => true,
                'credit' => $credit
            ]);
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id)
    {
        $credit = Credit::find($id);
        
        if($credit->delete()) {
            return response()->json([
                'success' => true,
                'message' => 'The credit where id '. $id . ' was deleted'
            ]);
        } else {
            return response()->json([
                'success' => false,
                'message' => 'The credit '. $id . ' cant be deleted'
            ]);
        }

        
    }
}
