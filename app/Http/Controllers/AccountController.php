<?php

namespace App\Http\Controllers;

use JWTAuth;
use App\Account;
use Illuminate\Http\Request;

class AccountController extends Controller
{
    /**
     * @var
     */
    protected $user;

    /**
     * AccountController constructor.
     */
    public function __construct()
    {
        $this->user = JWTAuth::parseToken()->authenticate();
    }

    /**
     * @return mixed
     */
    public function index()
    {
        $accounts = $this->user->accounts()->get(['id','name'])->toArray();

        return $accounts;
    }


    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        $account = $this->user->accounts()->find($id);

        if (!$account) {
            return response()->json([
                'success' => false,
                'message' => 'Désolé le compte ' . $id . ' na pas été trouvé.'
            ], 400);
        }

        return $account;
    }


    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required'
        ]);

        $account = new Account();
        $account->name = $request->name;

        if ($this->user->accounts()->save($account)) {
            return response()->json([
                'success' => true,
                'id' => $account->id,
                'name' => $account->name,
            ]);
            return $account;
        }
        else {
            return response()->json([
                'success' => false,
                'message' => 'Délosé le compte ne peux pas être ajouté.'
            ], 500);
        }
    }


    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, $id)
    {
        $account = $this->user->accounts()->find($id);

        if (!$account) {
            return response()->json([
                'success' => false,
                'message' => 'Délésolé le compte ' . $id . ' na pas été trouvé.'
            ], 400);
        }

        $updated = $account->fill($request->all())->save();

        if ($updated) {
            return response()->json([
                'success' => true
            ]);
        } else {
            return response()->json([
                'success' => false,
                'message' => 'Désolé, le compte ne peux pas être mis à jour.'
            ], 500);
        }
    }


    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id)
    {
        $account = $this->user->accounts()->find($id);

        if (!$account) {
            return response()->json([
                'success' => false,
                'message' => 'Désolé, le compte ' . $id . ' na pas été trouvé.'
            ], 400);
        }

        if ($account->delete()) {
            return response()->json([
                'success' => true
            ]);
        } else {
            return response()->json([
                'success' => false,
                'message' => 'le compte ne peux pas être supprimé.'
            ], 500);
        }
    }
}
