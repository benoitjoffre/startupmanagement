<?php

namespace App\Http\Controllers;

use JWTAuth;
use App\User;
use Illuminate\Http\Request;
use Tymon\JWTAuth\Exceptions\JWTException;
use App\Http\Requests\RegistrationFormRequest;
use Illuminate\Support\Facades\DB;


class ApiController extends Controller
{

   
    /**
     * @var bool
     */
    public $loginAfterSignUp = true;

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function login(Request $request)
    {
        $input = $request->only('email', 'password');
        $token = null;

        if(!$token = JWTAuth::attempt($input)) {
            return response()->json([
                'success' => false,
                'message' => 'email ou mot de passe invalide',
            ], 401);
        }
        return response()->json([
            'success' => true,
            'token' => $token,
        ]);
    }

     /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */

     public function logout(Request $request)
     {
        $this->validate($request, [
            'token' => 'required'
        ]);

        try {
            JWTAuth::invalidate($request->token);

            return response()->json([
                'success' => true,
                'message' => 'Vous avez été déconnecté avec succès'
            ]);
        } catch (JWTException $exception) {
            return response()->json([
                'success' => false,
                'message' => 'Désolé, vous ne pouvez pas être déconnecté'
            ], 500);
        }
     }

     /**
     * @param RegistrationFormRequest $request
     * @return \Illuminate\Http\JsonResponse
     */

    public function register(RegistrationFormRequest $request)
    {
        $user = new User();
        $user->name = $request->name;
        $user->email = $request->email;
        $user->password = bcrypt($request->password);
        $user->save();
 
        if ($this->loginAfterSignUp) {
            return $this->login($request);
        }
 
        return response()->json([
            'success' => true,
            'data' => $user
        ], 200);
    }


    public function getAuthUser(Request $request)
    {
        $this->validate($request, [
            'token' => 'required'
        ]);
 
        $user = JWTAuth::authenticate($request->token);
 
      
        return response()->json([
            'success' => true,
            'user' => $user
        ], 200);
    }
}
