<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Account;
use App\Debit;
use App\Http\Requests\DebitFormRequest;


class DebitController extends Controller
{
  

    public function index() 
    {
        $debits = Debit::with('account')->get();

        return response()->json([
            'success' => true,
            'debits'  => $debits,
        ], 200);
    }

    /**
     * @param DebitFormRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(DebitFormRequest $request, $accountId)
    {
        $debit = new Debit();
        $debit->desc = $request->desc;
        $debit->amount = $request->amount;
        $debit->startDate = $request->startDate;

        // Get the account by accountId in url
        $account = Account::find($accountId);

        $debit->account()->associate($account);
        
        $debit->save();

            return response()->json([
                'success' => true,
                'debit' => $debit
            ]);
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id)
    {
        $debit = Debit::find($id);
        
        if($debit->delete()) {
            return response()->json([
                'success' => true,
                'message' => 'The debit where id '. $id . ' was deleted'
            ]);
        } else {
            return response()->json([
                'success' => false,
                'message' => 'The debit '. $id . ' cant be deleted'
            ]);
        }

        
    }
}