<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Credit;
use App\Debit;

class Account extends Model
{
     /**
     * @var string
     */
    protected $table = 'accounts';

    /**
     * @var array
     */
    protected $guarded = [];


    public function credits() 
    {
        return $this->hasMany(Credit::class);
    }

    public function debits() 
    {
        return $this->hasMany(Debit::class);
    }
}
