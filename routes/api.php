<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('login', 'ApiController@login');
Route::post('register', 'ApiController@register');


// If authenticated
Route::group(['middleware' => 'auth.jwt'], function () {
    Route::get('logout', 'ApiController@logout');
    Route::get('user', 'ApiController@getAuthUser');
    
    // Accounts
    Route::get('accounts', 'AccountController@index');
    Route::get('accounts/{id}', 'AccountController@show');
    Route::post('accounts', 'AccountController@store');
    Route::put('accounts/{id}', 'AccountController@update');
    Route::delete('accounts/{id}', 'AccountController@destroy');

    // Credits
    Route::get('credits', 'CreditController@index');
    Route::post('credits/account/{accountId}', 'CreditController@store');
    Route::delete('credits/{id}', 'CreditController@destroy');

    // Debits

    Route::get('debits', 'DebitController@index');
    Route::post('debits/account/{accountId}', 'DebitController@store');
    Route::delete('debits/{id}', 'DebitController@destroy');
});