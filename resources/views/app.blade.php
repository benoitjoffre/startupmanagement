<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>StartupManagement</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
        <script src="https://kit.fontawesome.com/10bcb49af4.js" crossorigin="anonymous"></script>
        
    </head>
    <body>
	<h1 class="text-center">API StartupManagement</h1>        
    </body>
</html>
